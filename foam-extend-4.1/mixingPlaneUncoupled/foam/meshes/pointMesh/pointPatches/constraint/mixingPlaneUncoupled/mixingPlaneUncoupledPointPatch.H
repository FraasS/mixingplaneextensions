/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     4.1
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

Class
    mixingPlaneUncoupledPointPatch

Description
    Dummy mixingPlane patch for post-processing.  No functionality built in!
 
    Copy of mixingPlanePointPatch.H

Author
    Stefan Fraas, IHS, 2020.  All rights reserved

SourceFiles
    mixingPlaneUncoupledPointPatch.C

\*---------------------------------------------------------------------------*/

#ifndef mixingPlaneUncoupledPointPatch_H
#define mixingPlaneUncoupledPointPatch_H

#include "coupledFacePointPatch.H"
#include "mixingPlaneUncoupledPolyPatch.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                      Class mixingPlaneUncoupledPointPatch Declaration
\*---------------------------------------------------------------------------*/

class mixingPlaneUncoupledPointPatch
:
    public coupledFacePointPatch
{
    // Private data

        //- Local reference cast into the cyclic patch
        const mixingPlaneUncoupledPolyPatch& mixingPlanePolyPatch_;


    // Private Member Functions

        //- Disallow default construct as copy
        mixingPlaneUncoupledPointPatch(const mixingPlaneUncoupledPointPatch&);

        //- Disallow default assignment
        void operator=(const mixingPlaneUncoupledPointPatch&);


        //- Initialise the calculation of the patch geometry
        virtual void initGeometry()
        {}

        //- Calculate mesh points
        virtual void calcGeometry()
        {}

        //- Correct patches after moving points
        virtual void movePoints()
        {}

        //- Initialise the update of the patch topology
        virtual void initUpdateMesh();

        //- Update of the patch topology
        virtual void updateMesh();


public:

    typedef pointBoundaryMesh BoundaryMesh;


    //- Runtime type information
    TypeName(mixingPlaneUncoupledPolyPatch::typeName_());


    // Constructors

        //- Construct from components
        mixingPlaneUncoupledPointPatch
        (
            const polyPatch& patch,
            const pointBoundaryMesh& bm
        );


    // Destructor

        virtual ~mixingPlaneUncoupledPointPatch();


    // Member Functions

        // Access

            //- Return true because this patch is coupled
            virtual bool coupled() const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
